﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerScript : MonoBehaviour
{

    private Rigidbody2D rb;
    public Transform groundCheck;
    public LayerMask whatIsGround;
    public Animator animator;
    public Transform goal1;
    public Transform goal2;

    private bool isGrounded;
    private bool isOkayToJump = false;
    private bool isMovingRight = true;
    private bool isDead = false;

    public float waitForDeath = 3;
    public float checkRadius;
    public float jumpForce;
    public float speed = 5;
    public float health = 500;
    float horizontalMove = 0f;

    public int life = 3;

    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        print(isGrounded);
    }
    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKey(KeyCode.D))
        {
            if (isMovingRight)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                isMovingRight = false;
            }
        }
        if (Input.GetKey(KeyCode.A))
        {
            if (!isMovingRight)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                isMovingRight = true;
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("IsCrouching", true);
        }
        else
        {
            animator.SetBool("IsCrouching", false);
        }
        if(Input.GetMouseButton(0))
        {
            FindObjectOfType<AudioManager>().Play("SwordSlash");
            animator.SetBool("IsAttacking", true);
            Attack();
        }
        else
        {
            animator.SetBool("IsAttacking", false);
        }
        horizontalMove = Input.GetAxisRaw("Horizontal") * speed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
        if (Input.GetKey(KeyCode.Space))
        {
            animator.SetBool("IsJumping", isGrounded);
            Jump();
        }
        if (isMovingRight)
        {
            transform.Translate(-horizontalMove * Time.deltaTime, 0, 0);
        }
        if (!isMovingRight)
        {
            transform.Translate(horizontalMove * Time.deltaTime, 0, 0);
        }
        if(health<=0)
        {
            isDead = true;
            animator.SetBool("IsDead", isDead);
        }
        if(isDead)
        {
            waitForDeath -= Time.deltaTime;
        }
        if(waitForDeath<=0)
        {
            SceneManager.LoadScene("GameOver");
        }
        print("Health = " + health);
        LifeText.playerHealth = health;
    }
    public void Jump()
    {
        if (isGrounded)
        {
            rb.velocity = Vector2.up * jumpForce;
        }
        else
        return;
    }
    void Attack()
    {
        Debug.DrawRay(transform.position, transform.right*1, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right, 1);

        GameObject peo = hit.collider.gameObject;
        EnemyScript peop = peo.GetComponent<EnemyScript>();
        peop.Die();
        print("Hit = " + hit.collider.gameObject.name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Goal1")
        {
            transform.position = goal1.transform.position;
        }
        if (collision.gameObject.tag == "Goal2")
        {
            transform.position = goal2.transform.position;
        }
        if (collision.gameObject.tag == "Goal3")
        {
            SceneManager.LoadScene("GameOver");
        }

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Spikes")
        {
            health -= 20 * Time.deltaTime;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            health -= 50;
        }
        if(collision.gameObject.tag == "Death")
        {
            isDead = true;
            animator.SetBool("IsDead", isDead);

        }
    }
}
